import uuid
import logging
import json
from cryptography.fernet import Fernet

cookiename = 'amongus'

def has(request):
    return request.cookies.get(cookiename)

class Cookie:
    def __init__(self, request):
        self._key = self._loadKey()
        self._fernet = Fernet(self._key)
        self.state = self._get(request)

    def _getJson(self, request):
        if has(request):
            amongus = request.cookies.get(cookiename)
            return self._fernet.decrypt(bytes(amongus, 'utf-8'))
        return '{}'

    def delete(self, response):
        response.set_cookie(cookiename, '', expires=0)
    
    def add(self, response):
        self.setUuid()
        logging.warning(json.dumps(self.state))
        encrypted_json = self._fernet.encrypt(bytes(json.dumps(self.state), 'utf-8'))
        response.set_cookie(cookiename, encrypted_json, max_age=60*60*24*90)

    def _get(self, request):
        rawjson = self._getJson(request)
        logging.warning(rawjson)
        return json.loads(rawjson)

    def _loadKey(self):
        return open("secret.key", "rb").read()
    
    def getJson(self):
        return json.dumps(self.state)
    
    def setUuid(self):
        if self.state == None:
            self.state = {}
        if not 'uuid' in self.state:
            self.state['uuid'] = str(uuid.uuid1())
    
    def pathTrue(self, path):
        if path in self.state:
            if self.state[path] == "true":
                return True
        return False

    def getLocationFoundPath(self, location):
        return "%s/found" % (location)

    def getLocationCompletePath(self, location):
        return "%s/complete" % (location)

    def getLocationFound(self, location):
        return self.pathTrue(self.getLocationFoundPath(location))

    def getLocationComplete(self, location):
        return self.pathTrue(self.getLocationCompletePath(location))

    def setLocationFound(self, location):
        self.state[self.getLocationFoundPath(location)] = "true"

    def setLocationComplete(self, location):
        self.state[self.getLocationCompletePath(location)] = "true"
    
