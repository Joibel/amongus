from flask import Flask, request, make_response, render_template, redirect, url_for
import logging
import cookie
import locations

app = Flask(__name__)

def checkCookie(request):
    return not cookie.has(request)

@app.route("/", methods=['GET','POST'])
def holding():
    return redirect("/instructions")
#    return make_response(render_template('holding.html',
#                                         commonhead = getCommonHead(), header = getHeader(),  footer=""))

@app.route("/map", methods=['GET','POST'])
def map():
    cookieThing = cookieCheck(request)
    if not cookieThing is None:
        return cookieThing
    ck = cookie.Cookie(request)
    resp = make_response(render_template('map.html',
                                         mapoverlay = getMapOverlay(ck),
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    ck.add(resp)
    return resp

@app.route("/restart")
def restart():
    ck = cookie.Cookie(request)
    resp = redirect("/map")
    ck.delete(resp)
    return resp

@app.route("/unfound", methods=['GET','POST'])
def unfound():
    cookieThing = cookieCheck(request)
    if not cookieThing is None:
        return cookieThing
    ck = cookie.Cookie(request)
    html = make_response(render_template('unfound.html',
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    ck.add(html)
    return html

@app.route("/instructions", methods=['GET','POST'])
def instructions():
    cookieThing = cookieCheck(request)
    if not cookieThing is None:
        return cookieThing
    ck = cookie.Cookie(request)
    
    html = make_response(render_template('instructions.html',
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    ck.add(html)
    return html

#@app.route("/cookies", methods=['GET','POST'])
def cookieCheck(request):
    if cookie.has(request):
        return None
    if request.method == 'POST':
        if request.form.get('likecookies') == 'Yes':
            return None
        if request.form.get('nocookies') == 'No':
            return redirect("https://www.ascensionsouthampton.co.uk/what-we-do/youth/")
    html = make_response(render_template('cookies.html',
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    return html

@app.route("/policy")
def policy():
    html = make_response(render_template('policy.html',
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    return html

@app.route("/json", methods=['GET','POST'])
def send_json():
    cookieThing = cookieCheck(request)
    if not cookieThing is None:
        return cookieThing
    ck = cookie.Cookie(request)
    return ck.getJson()

@app.route("/location/<id>", methods=['GET','POST'])
def location(id=None):
    cookieThing = cookieCheck(request)
    if not cookieThing is None:
        return cookieThing
    if id == None:
        app.logger.warning("Location None")
        abort(404)
    id = id.lower()
    if id in locations.Locations:
        ck = cookie.Cookie(request)
        state = ck.setLocationFound(id)
        wrongAnswer = ''
        if request.method == 'POST':
            if 'answer' in request.form:
                if request.form['answer'].lower() == locations.Locations[id]['answer']:
                    ck.setLocationComplete(id)
                else:
                    app.logger.warning("Wanted %s but got %s" % (
                        locations.Locations[id]['answer'],
                        request.form['answer'].lower()))
                    wrongAnswer = "<p><b>Sorry, that's the wrong answer</b></p>"
        if ck.getLocationComplete(id):
            html = make_response(render_template('answer.html',
                                                 location = locations.Locations[id]['location'],
                                                 answer = locations.Locations[id]['answer'].capitalize(),
                                                 response = locations.Locations[id]['response'],
                                                 commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
        else:
            html = make_response(render_template('question.html',
                                                 location = locations.Locations[id]['location'],
                                                 question = locations.Locations[id]['question'],
                                                 wrongAnswer = wrongAnswer,
                                                 commonhead = getCommonHead(), header = getHeader(), footer=getFooter()))
        ck.add(html)
        return html
    app.logger.warning("Location %s" % (id))
    return 'Invalid location', 404

@app.route('/favicon.ico')
def favicon():
    return redirect("/media/favicon.ico")
    
def getHeader():
    return render_template('header.html')

def getFooter():
    return render_template('footer.html')

def getCommonHead():
    return render_template('commonhead.html')

def getMapOverlay(ck):
    mapoverlay = ''
    for key, value in locations.Locations.items():
        if ck.getLocationComplete(key):
            img = 'star.png'
            location = "/location/%s" % (key)
            iconclass = 'mapicon'
        elif ck.getLocationFound(key):
            img = 'key.png'
            location = "/location/%s" % (key)
            iconclass = 'mapicon'
        else:
            img = 'map-pin.png'
            location = "/unfound"
            iconclass = 'mappin'
        x = int(value['coordsx']) * 100 / 2688
        y = int(value['coordsy']) * 100 / 3924
        mapoverlay += "<a href=\"%s\"><img src=\"/media/%s\" alt=\"%s\" style=\"left: %s%%; top: %s%%;\" class=\"%s\"></a>" % (location, img, value['location'], x, y, iconclass)
    return mapoverlay

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0')
