from flask import Flask, request, make_response, render_template, redirect, url_for
import logging
import cookie
import locations

app = Flask(__name__)

@app.route("/redeem", methods=['GET','POST'])
def map():
    cookieThing = cookieCheck(request)
    if not cookieThing is None:
        return cookieThing
    ck = cookie.Cookie(request)
    resp = make_response(render_template('redeem.html',
                                         details = getDetails(ck),
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    ck.add(resp)
    return resp


#@app.route("/cookies", methods=['GET','POST'])
def cookieCheck(request):
    if cookie.has(request):
        return None
    if request.method == 'POST':
        if request.form.get('likecookies') == 'Yes':
            return None
        if request.form.get('nocookies') == 'No':
            return redirect("https://www.ascensionsouthampton.co.uk/what-we-do/youth/")
    html = make_response(render_template('cookies.html',
                                         commonhead = getCommonHead(), header = getHeader(),  footer=getFooter()))
    return html



def getHeader():
    return render_template('header.html')

def getFooter():
    return render_template('footer.html')

def getCommonHead():
    return render_template('commonhead.html')

def getDetails(ck):
    complete = 0
    incomplete = 0
    unvisited = 0
    locationDetail = '<table></tr><th>Location</th><th>Success<th></tr>'
    locationWord = ''
    for key, value in locations.Locations.items():
        if ck.getLocationComplete(key):
            complete += 1
            locationWord = 'Completed successfully'
        elif ck.getLocationFound(key):
            incomplete += 1
            locationWord = 'Visited but not answered'
        else:
            unvisited += 1
            locationWord = 'Not visited'
        locationDetail += "<tr><td>%s</td><td>%s</td></td>" % (value['location'], locationWord)
    locationDetail += '</table>'
    retval=''
    if complete>0:
        retval+="<h3>Completed successfully %d</h3>" % (complete)
    if incomplete>0:
        retval+="<h3>Visited but not answered %d</h3>" % (incomplete)
    if unvisited>0:
        retval+="<h3>Not visited %d</h3>" % (unvisited)
    retval += locationDetail
    return retval

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0')
