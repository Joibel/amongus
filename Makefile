.PHONY: container_app container_media local container_media_push container_app_push

IMAGE_APP := docker.io/joibel/amongus:latest
IMAGE_MEDIA := docker.io/joibel/amongusmedia:latest
IMAGE_REDEEM := docker.io/joibel/amongusredeem:latest
PODMAN_PARAMS := --events-backend=file
NAMESPACE := amongus

restart_pods:  container_push
	kubectl get deployments -o name -n $(NAMESPACE) | xargs kubectl rollout restart -n $(NAMESPACE)

container_push: container_app_push container_media_push container_redeem_push

container_app_push: container_app
	podman push $(IMAGE_APP) $(PODMAN_PARAMS)

container_app: app/*
	podman build . -t $(IMAGE_APP) $(PODMAN_PARAMS) -f Dockerfile.app

container_media_push: container_media
	podman push $(IMAGE_MEDIA) $(PODMAN_PARAMS)

container_media: media/*
	podman build . -t $(IMAGE_MEDIA) $(PODMAN_PARAMS) -f Dockerfile.media

container_redeem_push: container_redeem
	podman push $(IMAGE_REDEEM) $(PODMAN_PARAMS)

container_redeem: redeem/*
	podman build . -t $(IMAGE_REDEEM) $(PODMAN_PARAMS) -f Dockerfile.redeem

local:
	cd app && env FLASK_APP=app.py flask run

localredeem:
	cd redeem && env FLASK_APP=app.py flask run

